
const url = 'https://ajax.test-danit.com/api/swapi/films'

fetch(url)
    .then(response => response.json())
    .then(films => {

        films.map (film=> {
            let ul = 
            `<ul>
            <li>Назва: ${film.name}</li>
            <li>Епізод: ${film.episodeId}</li>
            <li>Опис: ${film.openingCrawl}</li>
            </ul>`
            const div = document.createElement('div')
            div.setAttribute('data-episodeId', film.episodeId)
            div.innerHTML = ul
            document.body.append(div)
            addCharacters(film)
        })
    })
    .catch(error => console.log(error))


function addCharacters (film)  {
    const characters  = (film.characters).map(url => fetch(url).then(response => response.json()))
    return Promise.all(characters)
        .then(characters => {
           let li=''
          characters.map(character => {
            li += `<li>Актор: ${character.name}</li>`   
        }
         )
            const div = document.querySelector(`[data-episodeId='${film.episodeId}']`)
            div.insertAdjacentHTML('beforeend', `<ul>${li}</ul>`);
        })
        .catch(error => console.log(error))
}

